extends VBoxContainer

func get_average() -> float:
	var count = self.get_node("Margin/Rolls/Count").get_line_edit().get_text()
	var faces = self.get_node("Margin/Rolls/Faces").get_selected_id()
	var faces_average = 0

	match faces:
		0: # D4
			faces_average = 2.5
		1: # D6
			faces_average = 3.5
		2: # D8
			faces_average = 4.5
		3: # D10
			faces_average = 5.5
		4: # D12
			faces_average = 6.5
		_:
			print("Invalid number of faces")
	
	return count.to_int() * faces_average
