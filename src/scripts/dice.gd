extends VBoxContainer

var die_scene = preload("res://src/scenes/die.tscn")

func _on_new_die_pressed():
	var die = die_scene.instantiate()
	add_child(die)
