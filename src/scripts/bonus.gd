extends VBoxContainer

func _on_type_item_selected(index):
	var description = self.get_node("DescriptionMargin")
	if index == 3:
		description.visible = true
	else:
		description.visible = false
