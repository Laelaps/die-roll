extends Button

func get_dice_average() -> float:
	var average = 0
	for die in %Dice.get_children():
		average += die.get_average()
	
	return average


func get_bonus_total() -> int:
	var total = 0
	for bonus in %Bonuses.get_children():
		var value = bonus.get_node("ModifierMargin/Modifier/Value")\
			.get_line_edit()\
			.get_text()
		
		total += value.to_int()

	return total

func _on_get_damage_pressed():
	var total = 0
	total += get_dice_average()
	total += get_bonus_total()
	
	%Total.set_text(str(total))
