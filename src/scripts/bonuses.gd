extends VBoxContainer

var bonus_scene = preload("res://src/scenes/bonus.tscn")

func _on_new_damage_bonus_pressed():
	var bonus = bonus_scene.instantiate()
	add_child(bonus)
